<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Social Signin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <!-- <link rel="stylesheet" type="text/css" media="screen" href="main.css" /> -->
        
    </head>
    <body>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                appId      : '428056617665764',
                cookie     : true,
                xfbml      : false,
                version    : 'v3.0'
                });
                
                FB.AppEvents.logPageView();

                // This method checks the login status of user.
                checkLoginState();
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            


        </script>
        <div id="fb-root"></div>
        <div class="container">
            <h1>Welcome to Treasure Island.</h1>
            <p>To unclock the hidden treasures please sign in yourself.</p>
            <button class="btn btn-social btn-facebook" id="btn-facebook-login"><span class="fa fa-facebook"></span> Login with Facebook</button>

            <div class="row">
                <div class="col-lg-6">
                    <div id="message"></div>
                    
                </div>
                <div class="col-lg-6">
                    <div id="profile-image"></div>
                    <br>
                    <div id="other-btns"></div>
                </div>
            </div>
            
        </div>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>