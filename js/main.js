var loginButton = document.getElementById("btn-facebook-login");
var messageBox = document.getElementById("message");
var profilePicBox = document.getElementById("profile-image");
var access_token;
loginButton.addEventListener("click", function () {
    login();
});

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

function statusChangeCallback(response) {
    console.log(response);
    if (response.status === "not_authorized") {
        login();
    } else if (response.status === "connected") {
        handleSuccessfulLogin(response);
    }
}

function fetchUserInfo(uid) {
    FB.api('/me', function (response) {
        console.log("userInfo", response);
        messageBox.innerHTML = 'Good to see you, <b>' + response.name + '</b>.';
        console.log('Good to see you, ' + response.name + '.');
        var profile_image = document.createElement("img");
        profile_image.src = "http://graph.facebook.com/" + response.id + "/picture?height=200&width=200";
        profilePicBox.appendChild(profile_image);
    });
}

function handleLogout() {
    FB.logout(function (response) {
        console.log(response);
        loginButton.style.display = "block";
        document.getElementById("btn-logout").remove();
        messageBox.innerHTML = "";
        profilePicBox.innerHTML = "";
    });
}

function createLogout() {
    var btn_logout = document.createElement("button");
    btn_logout.innerHTML = "Logout";
    btn_logout.id = "btn-logout";
    btn_logout.className = "btn btn-secondary";

    btn_logout.addEventListener("click", function (e) {
        handleLogout();
    });

    document.getElementById("other-btns").appendChild(btn_logout);
    console.log("creating logout button");
}

function handleSuccessfulLogin(response) {
    console.log("handleSuccessfulLogin", response.authResponse);
    access_token = response.authResponse.accessToken;
    loginButton.style.display = "none";
    fetchUserInfo(response.authResponse.userID);
    createLogout();
    createDisconnectButton(response.authResponse.accessToken);
}

function login() {
    FB.login(function (response) {
        console.log(response);
        if (response.authResponse && response.status === "connected") {
            console.log('Welcome!  Fetching your information.... ');
            handleSuccessfulLogin(response);

        } else {
            console.log('User cancelled login or did not fully authorize.');
        }
    });
}

function createDisconnectButton(accessToken){
    var btn_disconnect = document.createElement("button");
    btn_disconnect.innerHTML = "Disconnect from facebook";
    btn_disconnect.id = "btn-disconnect";
    btn_disconnect.className = "btn btn-danger";

    btn_disconnect.addEventListener("click", function (e) {
        disconnect(accessToken);
    });

    document.getElementById("other-btns").appendChild(btn_disconnect);
    console.log("creating disconnect button");
}


function disconnect(accessToken) {
    var url = "https://graph.facebook.com/v3.0/me?access_token="+accessToken;
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", url , true);
    xhr.onload = function () {
        var response = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "200") {
            console.table(response);
            checkLoginState();
        } else {
            console.error(response);
        }
    }
    xhr.send(null);
}